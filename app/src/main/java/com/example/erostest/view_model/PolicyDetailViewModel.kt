package com.example.erostest.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.erostest.Model.PolicyDetails
import com.example.erostest.managers.ApiManager
import com.example.erostest.network.configuration.DataResponse

class PolicyDetailViewModel : ViewModel() {

    fun getPolicyDetails(membersCount: Int): LiveData<DataResponse<PolicyDetails>> {
        return ApiManager.getPolicyDetails(membersCount)
    }
}