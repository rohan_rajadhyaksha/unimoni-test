package com.example.erostest.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.erostest.Model.Result
import com.example.erostest.managers.MovieManager
import com.example.erostest.network.configuration.DataResponse
import com.example.erostest.network.helper.AppExecutors
import com.example.erostest.pagination.MoviesDataSourceFactory

class MoviesViewModel : ViewModel() {

    var topRatedMovieList: LiveData<PagedList<Result>>? = null
    var loadingStatusLiveData: LiveData<DataResponse.Status>? = null

    init {
        initializePaging()
    }

    private fun initializePaging() {

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(10)
            .setPageSize(10).build()

        var moviesDataSourcefactory = MoviesDataSourceFactory()
        topRatedMovieList = LivePagedListBuilder(moviesDataSourcefactory, pagedListConfig)
            .setFetchExecutor(AppExecutors().mainThread())
            .build()

        loadingStatusLiveData = Transformations.switchMap(moviesDataSourcefactory.liveData) {
            it.dataLoadingStatus
        }


    }

    fun getFavouriteMovies(): LiveData<List<Result>> {
        return MovieManager.getFavouriteMovies()
    }

    fun updateMovie(movieId: Int, isFavourite: Int) {
        MovieManager.updateMovie(movieId, isFavourite)
    }


}