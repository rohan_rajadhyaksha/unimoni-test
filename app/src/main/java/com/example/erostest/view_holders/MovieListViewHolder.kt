package com.example.erostest.view_holders

import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.erostest.Model.Result
import com.example.erostest.R
import com.example.erostest.constants.BaseUrlConstants
import com.example.erostest.database.DbConstants
import com.example.erostest.listeners.OnFavouriteIconClickListener

class MovieListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(movieResult: Result?, listener: OnFavouriteIconClickListener? = null) {
        movieResult?.let {
            val tvTitle = itemView.findViewById(R.id.tvTitle) as TextView
            val tvDescription = itemView.findViewById(R.id.tvDescription) as TextView
            val ivMovieBanner = itemView.findViewById(R.id.ivBannerImage) as ImageView
            val ivFav = itemView.findViewById(R.id.ivFav) as ImageView

            if (it.favourite == DbConstants.IS_FAVOURITE) {
                ivFav.setImageResource(R.drawable.ic_fav_red)
            } else {
                ivFav.setImageResource(R.drawable.ic_fav)
            }
            if (!TextUtils.isEmpty(it.poster_path))
                Glide.with(ivFav.context).load(BaseUrlConstants.BASE_URL_FOR_BANNER + it.poster_path).into(ivMovieBanner)

            ivFav.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    listener?.favIconClicked(it.id, it.favourite)
                }

            })


            tvTitle.text = movieResult.title
            tvDescription.text = movieResult.overview
        }


    }
}