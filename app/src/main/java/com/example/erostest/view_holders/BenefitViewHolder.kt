package com.example.erostest.view_holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.erostest.BenefitEnum
import com.example.erostest.Model.Benefit
import com.example.erostest.R

class BenefitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)  {

    @JvmField @BindView(R.id.ivBenifitIcon) protected var ivBenefit:ImageView?=null
    @JvmField @BindView(R.id.tvBenifitTitle)protected var tvBenefitTitle :TextView?=null
    @JvmField @BindView(R.id.tvBenifitSubTitle)protected var tvBenefitSubTitle:TextView?=null
    @JvmField @BindView(R.id.tvSaveUpTo)protected var tvSaveUpTo:TextView?=null
    init {
        ButterKnife.bind(this, itemView)
    }

    fun bind(benefit: Benefit) {
        val benefitEnum :BenefitEnum? = BenefitEnum.getBenefitByBenefitName(benefit.benefit_icon_local)
        benefitEnum?.let {
            ivBenefit?.let {ivBenefit->
                ivBenefit.setImageResource(it.typeIcon)
            }
        }
        tvBenefitTitle?.let {
            it.text=benefit.benefit_title
        }
        tvBenefitSubTitle?.let {
            it.text=benefit.benefit_sub_title
        }

    }

    fun bind(saveUpTo:Int){

        tvSaveUpTo?.let {
            it.text= "Save Up To $saveUpTo"
        }

    }
}