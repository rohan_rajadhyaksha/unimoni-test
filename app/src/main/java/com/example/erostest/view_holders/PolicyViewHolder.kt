package com.example.erostest.view_holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.erostest.Model.Plan
import com.example.erostest.R
import com.example.erostest.adpaters.BenefitAdapter

class PolicyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @JvmField @BindView(R.id.tvBuyNowFor)
    protected var tvBuyNowFor: TextView? = null
    @JvmField @BindView(R.id.tvPolicyPrice)
    protected var tvPolicyPrice: TextView? = null
    @JvmField @BindView(R.id.tvPlanName)
    protected var tvPlanName: TextView? = null
    @JvmField @BindView(R.id.rvGrid)
    protected var rvGrid: RecyclerView? = null
    @JvmField @BindView(R.id.tvUpToMembers)
    protected var tvUpToMembers: TextView? = null

    init {
        ButterKnife.bind(this, itemView)
    }

    fun bind(plan: Plan, membersCount: Int) {
        tvBuyNowFor?.let {
            it.text = "Buy Now For " + plan.plan_cost
        }

        tvPolicyPrice?.let {
            it.text = plan.plan_cost.toString()
        }

        tvPlanName?.let {
            it.text = plan.plan_name
        }
        tvUpToMembers?.let {
            it.text = "Up To $membersCount family members"
        }

        rvGrid?.let {
            val gridLayoutManager = GridLayoutManager(it.context, 2)
            val benefirAdapter = BenefitAdapter(plan.benefits, plan.plan_save_upto)
            gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    when (benefirAdapter.getItemViewType(position)) {
                        BenefitAdapter.FOOTER_VIEW_TYPE -> {
                            if (plan.benefits.size % 2 == 0) {
                                return 2
                            } else {
                                return 1
                            }
                        }
                        else -> {
                            return 1
                        }

                    }
                }

            }
            it.layoutManager = gridLayoutManager

            it.adapter =benefirAdapter
        }

    }


}