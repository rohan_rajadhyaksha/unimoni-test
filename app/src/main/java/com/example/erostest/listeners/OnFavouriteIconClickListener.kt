package com.example.erostest.listeners

interface OnFavouriteIconClickListener {

    fun favIconClicked(movieId: Int, tag: Int)
}