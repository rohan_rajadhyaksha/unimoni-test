package com.example.erostest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.erostest.R
import com.example.erostest.adpaters.MoviesPagedListAdapter
import com.example.erostest.database.DbConstants
import com.example.erostest.listeners.OnFavouriteIconClickListener
import com.example.erostest.network.configuration.DataResponse
import com.example.erostest.utils.AppUtils
import com.example.erostest.view_model.MoviesViewModel

class TopRatedMoviesFragment : Fragment(), OnFavouriteIconClickListener {


    var moviesViewModel: MoviesViewModel? = null
    var rvMovies: RecyclerView? = null

    companion object {
        fun newInstance(): TopRatedMoviesFragment {
            return TopRatedMoviesFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_top_rated_movies, container, false)
        rvMovies = view.findViewById(R.id.rvMovies) as RecyclerView
        init()
        return view;
    }

    private fun init() {
        moviesViewModel = ViewModelProviders.of(this).get(MoviesViewModel::class.java)
        rvMovies?.layoutManager = GridLayoutManager(activity, 2)
        val adapter = MoviesPagedListAdapter(this)
        rvMovies?.adapter = adapter

        moviesViewModel?.topRatedMovieList?.observe(this, Observer { adapter.submitList(it) })
        moviesViewModel?.loadingStatusLiveData?.observe(this, Observer {
            when (it) {
                DataResponse.Status.LOADING -> {
                    AppUtils.showProgressDialog(activity, false)
                }
                else -> {
                    AppUtils.hideProgressDialog()
                }

            }
        })


    }

    override fun favIconClicked(movieId: Int, tag: Int) {
        if (tag == DbConstants.IS_FAVOURITE) {
            moviesViewModel?.updateMovie(movieId, DbConstants.NOT_FAVOURITE)
        } else {
            moviesViewModel?.updateMovie(movieId, DbConstants.IS_FAVOURITE)
        }
    }

}