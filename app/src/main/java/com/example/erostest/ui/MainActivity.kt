package com.example.erostest.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.erostest.Model.Plan
import com.example.erostest.Model.PolicyDetails
import com.example.erostest.R
import com.example.erostest.adpaters.PolicyAdapter
import com.example.erostest.network.configuration.DataResponse
import com.example.erostest.utils.AppUtils
import com.example.erostest.view_model.PolicyDetailViewModel

class MainActivity : AppCompatActivity(), View.OnClickListener {


    @JvmField @BindView(R.id.btnTwoMembers)
    protected var btnTwoMembers: Button? = null


    @JvmField @BindView(R.id.btnFourMembers)
    protected var btnFourMembers: Button? = null


    @JvmField @BindView(R.id.rvPolicies)
    protected var rvPolicies: RecyclerView? = null

    var policyDetailsViewModel: PolicyDetailViewModel? = null
    var policyDetailsLiveData: LiveData<DataResponse<PolicyDetails>>? = null
    var planList: MutableList<Plan>? = null
    var policyAdapter: PolicyAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        initFields()
        initListeners()
        setAdapter()


    }

    private fun initListeners() {
        /*btnTwoMembers=findViewById(R.id.btnTwoMembers)
        btnFourMembers=findViewById(R.id.btnFourMembers)*/
        btnFourMembers?.setOnClickListener(this)
        btnTwoMembers?.setOnClickListener(this)
    }

    private fun initFields() {
        policyDetailsViewModel = ViewModelProviders.of(this).get(PolicyDetailViewModel::class.java)
        rvPolicies?.layoutManager = LinearLayoutManager(this)
    }

    private fun setAdapter(membersCount: Int=2) {
        if (null == policyAdapter) {
            policyAdapter = PolicyAdapter(planList)
            rvPolicies?.adapter = policyAdapter
        } else {
            policyAdapter?.notifyDataSet(planList,membersCount)
        }
    }

    private fun getPolicyDetails(membersCount: Int) {

        AppUtils.hideProgressDialog()
        policyDetailsLiveData?.removeObservers(this)

        policyDetailsViewModel?.let { viewModel ->
            AppUtils.showProgressDialog(this, false)
            policyDetailsLiveData = viewModel.getPolicyDetails(membersCount)
            policyDetailsLiveData?.let {
                it.observe(this, Observer { dataResponse ->
                    when (dataResponse.status) {
                        DataResponse.Status.LOADING -> {

                        }
                        DataResponse.Status.SUCCESS -> {
                            AppUtils.hideProgressDialog()
                            it.removeObservers(this)
                            val policyDetails = dataResponse.data
                            Log.d("Alpha", "Data received " + policyDetails?.plans?.size)
                            Toast.makeText(
                                this,
                                "Data received, No Of plans are " + policyDetails?.plans?.size,
                                Toast.LENGTH_SHORT
                            ).show()
                            policyDetails?.plans?.let {
                                planList = it
                                setAdapter(membersCount)
                            }
                        }
                        DataResponse.Status.ERROR -> {
                            AppUtils.hideProgressDialog()
                            it.removeObservers(this)
                            Log.d("Alpha", "error occured")
                        }

                    }
                })
            }

        }
    }

    override fun onClick(v: View?) {
        val id = v?.id
        when (id) {
            R.id.btnTwoMembers -> {
                btnTwoMembers?.setBackgroundResource(R.drawable.button_background_sky_blue)
                btnFourMembers?.setBackgroundResource(R.drawable.button_background_dark_blue)
                getPolicyDetails(2)
            }
            R.id.btnFourMembers -> {
                btnTwoMembers?.setBackgroundResource(R.drawable.button_background_dark_blue)
                btnFourMembers?.setBackgroundResource(R.drawable.button_background_sky_blue)
                getPolicyDetails(4)
            }
        }
    }


}
