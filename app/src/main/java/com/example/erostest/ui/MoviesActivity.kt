package com.example.erostest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.erostest.R
import com.example.erostest.adpaters.TabAdapter
import com.google.android.material.tabs.TabLayout


class MoviesActivity : AppCompatActivity() {

    private var adapter: TabAdapter? = null
    private var tabLayout: TabLayout? = null
    private var viewPager: ViewPager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        init();
    }

    fun init() {
        viewPager = findViewById(R.id.viewPager) as ViewPager
        tabLayout = findViewById(R.id.tabLayout) as TabLayout
        adapter = TabAdapter(supportFragmentManager)
        adapter?.let {
            it.addFragment(TopRatedMoviesFragment.newInstance(), getString(R.string.top_rated))
            it.addFragment(FavouriteMoviesFragment.newInstance(), getString(R.string.favourite))

        }
        viewPager?.adapter = adapter
        tabLayout?.setupWithViewPager(viewPager)

        val tabCount = tabLayout?.tabCount ?: 0
        for (i in 0..tabCount) {
            val view: TextView = LayoutInflater.from(this).inflate(R.layout.layout_custom_tab, null, false) as TextView
            tabLayout?.getTabAt(i)?.setCustomView(view);
        }



    }
}