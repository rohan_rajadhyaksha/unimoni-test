package com.example.erostest.utils.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import com.example.erostest.R


class ProportionImageView(context: Context, attributeSet: AttributeSet) : ImageView(context, attributeSet) {

    private val proportionValue: Float

    init {
        val typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.ProportionImageView, 0, 0)
        proportionValue = typedArray.getFloat(R.styleable.ProportionImageView_proportion_value, 1.0f)
        typedArray.recycle()
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val newHeightMeasureSpec =
            MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.EXACTLY)
        super.onMeasure(widthMeasureSpec, newHeightMeasureSpec)

    }

    override fun hasOverlappingRendering(): Boolean {
        return false
    }

}