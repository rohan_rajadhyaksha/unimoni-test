package com.example.erostest.utils;

import android.content.Context;
import com.example.erostest.utils.widget.LoadingDialog;

public class AppUtils {

    private static LoadingDialog progressDialog;

    public static void showProgressDialog(Context context, boolean isCancelable) {
        hideProgressDialog();
        if (context != null) {
            try {
                progressDialog = new LoadingDialog(context);
                progressDialog.setCanceledOnTouchOutside(true);
                progressDialog.setCancelable(isCancelable);
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
                progressDialog = null;
            } catch (Exception e) {
                progressDialog = null;
            }
        }
    }
}
