package com.example.erostest

import android.app.Application
import com.example.erostest.database.initDatabases

class ErosNowTestApplication : Application() {

    companion object {
        lateinit var application: ErosNowTestApplication
    }

    override fun onCreate() {
        super.onCreate()
        application = this
        initDatabases(this)
    }
}