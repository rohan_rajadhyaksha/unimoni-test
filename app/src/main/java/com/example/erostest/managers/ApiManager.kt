package com.example.erostest.managers

import androidx.lifecycle.LiveData
import com.example.erostest.Model.PolicyDetails
import com.example.erostest.Model.TopRatedMovieResponse
import com.example.erostest.network.configuration.DataResponse
import com.example.erostest.network.helper.UrlProvider
import com.faasos.foodx.library.network.configuration.ServiceProvider

object ApiManager {

    @JvmStatic
    fun getPolicyDetails(membersCount: Int): LiveData<DataResponse<PolicyDetails>> {
        return ServiceProvider.getApiService().getPolicyDetails(UrlProvider.getPolicyDetailsUrl(membersCount))
    }

    @JvmStatic
    fun getTopRatedMovies(pageNumber: Int): LiveData<DataResponse<TopRatedMovieResponse>> {
        return ServiceProvider.getApiService().getTopRatedMovies(UrlProvider.getTopRatedMovies(pageNumber))
    }
}