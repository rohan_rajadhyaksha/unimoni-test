package com.example.erostest.managers

import androidx.lifecycle.LiveData
import com.example.erostest.Model.Result
import com.example.erostest.database.MoviesDatabase
import com.example.erostest.network.helper.AppExecutors

object MovieDbManager {

    @Synchronized
    fun addMovieList(movieList: List<Result>) {
        MoviesDatabase.getInstance().moviesDao().addMovieResult(movieList)
    }

    @Synchronized
    fun getAllMovies(): LiveData<List<Result>> {
        return MoviesDatabase.getInstance().moviesDao().getAllMovies()
    }

    @Synchronized
    fun getAllFavouriteMovies(): LiveData<List<Result>> {
        return MoviesDatabase.getInstance().moviesDao().getAllFavouriteMovies()
    }

    @Synchronized
    fun updateMovie(movieId: Int, isFavourite: Int) {
        AppExecutors().diskIO().execute() {
            MoviesDatabase.getInstance().moviesDao().updateFavouriteMovie(movieId, isFavourite)
        }
    }
}