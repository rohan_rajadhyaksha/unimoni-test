package com.example.erostest.managers

import androidx.lifecycle.LiveData
import com.example.erostest.Model.Result
import com.example.erostest.Model.TopRatedMovieResponse
import com.example.erostest.network.configuration.DataResponse
import com.example.erostest.network.helper.NetworkDbBindingResource

object MovieManager {

    fun getTopRatedMovies(pageNumber: Int): LiveData<DataResponse<List<Result>>> {
        return object : NetworkDbBindingResource<TopRatedMovieResponse, List<Result>>() {
            override fun shouldRefreshData(data: List<Result>?): Boolean = true

            override fun shouldAlwaysFetchData(): Boolean = true

            override fun createCall(): LiveData<DataResponse<TopRatedMovieResponse>> {
                return ApiManager.getTopRatedMovies(pageNumber)
            }

            override fun storeApiResult(item: TopRatedMovieResponse) {
                MovieDbManager.addMovieList(item.results)
            }

            override fun loadFromDb(): LiveData<List<Result>> {
                return MovieDbManager.getAllMovies()
            }

        }.resultLiveData

    }


    fun getFavouriteMovies(): LiveData<List<Result>> {
        return MovieDbManager.getAllFavouriteMovies()
    }

    fun updateMovie(movieId: Int, isFavourite: Int) {
        MovieDbManager.updateMovie(movieId, isFavourite)
    }
}