package com.example.erostest.constants

object ErrorCodeConstants {
    const val WFH = -1
    const val NETWORK = 0
    const val BAD_REQUEST = 400
    const val UNAUTHORIZED_TOKEN = 401
    const val SERVER_BUG = 500
    const val SERVER_MAINTENANCE = 502


}

