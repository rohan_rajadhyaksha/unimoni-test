package com.example.erostest.constants

object BaseUrlConstants {

    const val BASE_URL_API = "https://api.themoviedb.org/3/"
    const val BASE_URL_FOR_BANNER = "http://image.tmdb.org/t/p/w500"
}