package com.example.erostest.adpaters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.erostest.Model.Result
import com.example.erostest.R
import com.example.erostest.listeners.OnFavouriteIconClickListener
import com.example.erostest.view_holders.MovieListViewHolder

class FavouriteMoviesAdapter(var favIconClickListener: OnFavouriteIconClickListener) :
    RecyclerView.Adapter<MovieListViewHolder>() {
    var favMovieList: List<Result>

    init {
        favMovieList = ArrayList()
    }

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        holder.bind(favMovieList[position], listener = favIconClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_top_rated_movies, parent, false)
        return MovieListViewHolder(view)
    }

    override fun getItemCount(): Int = favMovieList.size


    fun setFavMovieDataSet(movieList: List<Result>) {
        this.favMovieList = movieList
        notifyDataSetChanged()
    }
}