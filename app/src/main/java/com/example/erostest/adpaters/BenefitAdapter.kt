package com.example.erostest.adpaters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.erostest.Model.Benefit
import com.example.erostest.R
import com.example.erostest.view_holders.BenefitViewHolder

class BenefitAdapter(var benefitList: MutableList<Benefit>, var saveUpTo: Int = 0) : RecyclerView.Adapter<BenefitViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BenefitViewHolder {
        return if (viewType == ITEM_VIEW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_grid_item, parent, false)
            BenefitViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_grid_footer, parent, false)
            BenefitViewHolder(view)
        }

    }

    override fun getItemCount(): Int {
        if (saveUpTo > 0) {
            return benefitList?.size + 1
        } else {
            return benefitList.size
        }
    }

    override fun onBindViewHolder(holder: BenefitViewHolder, position: Int) {
        when (holder.itemViewType) {
            ITEM_VIEW_TYPE ->{
                holder.bind(benefitList[position])
            }

            FOOTER_VIEW_TYPE ->{
                holder.bind(saveUpTo)
            }

        }
    }

     override fun getItemViewType(position: Int): Int {
        return if (saveUpTo >0 && position == benefitList!!.size ) {
            FOOTER_VIEW_TYPE
        } else {
            ITEM_VIEW_TYPE
        }

    }




    companion object BenefitCompanion{
         const val ITEM_VIEW_TYPE = 1

         const val FOOTER_VIEW_TYPE = 2
    }
}