package com.example.erostest.adpaters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.erostest.Model.Plan
import com.example.erostest.R
import com.example.erostest.view_holders.PolicyViewHolder

class PolicyAdapter(var plansList:MutableList<Plan>?,var membersCount:Int=2) :RecyclerView.Adapter<PolicyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PolicyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_policy, parent, false)
        return PolicyViewHolder(view)
    }

    override fun getItemCount(): Int =plansList?.size?:0

    override fun onBindViewHolder(holder: PolicyViewHolder, position: Int) {
        holder.bind(plansList!![position],membersCount)
    }

    fun notifyDataSet(planList:MutableList<Plan>?,membersCount: Int=2){
        this.plansList?.clear()
        this.plansList=planList
        this.membersCount=membersCount
        notifyDataSetChanged()
    }





}