package com.example.erostest.adpaters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.erostest.Model.Result
import com.example.erostest.R
import com.example.erostest.listeners.OnFavouriteIconClickListener
import com.example.erostest.view_holders.MovieListViewHolder

class MoviesPagedListAdapter(val listener: OnFavouriteIconClickListener) :
    PagedListAdapter<Result, MovieListViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Result>() {


            override fun areItemsTheSame(@NonNull oldItem: Result, @NonNull newItem: Result): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(@NonNull oldItem: Result, @NonNull newItem: Result): Boolean {
                return oldItem == newItem
            }


        }
    }

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_top_rated_movies, parent, false)
        return MovieListViewHolder(view)
    }


}