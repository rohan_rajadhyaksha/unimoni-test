package com.example.erostest.database

object DbConstants {
    const val DB_NAME = "movies_db"
    const val DB_VERSION = 1

    const val MOVIE_TABLE = "Movie_Table"
    const val IS_FAVOURITE = 1
    const val NOT_FAVOURITE = 0

}