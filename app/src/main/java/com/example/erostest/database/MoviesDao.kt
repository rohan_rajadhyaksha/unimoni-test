package com.example.erostest.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.erostest.Model.Result

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMovieResult(result: List<Result>)

    @Transaction
    @Query(" SELECT * FROM " + DbConstants.MOVIE_TABLE)
    fun getAllMovies(): LiveData<List<Result>>

    @Transaction
    @Query(" SELECT * FROM " + DbConstants.MOVIE_TABLE + " WHERE favourite = " + DbConstants.IS_FAVOURITE)
    fun getAllFavouriteMovies(): LiveData<List<Result>>

    @Transaction
    @Query(" UPDATE " + DbConstants.MOVIE_TABLE + " SET favourite =:isFavourite WHERE id = :movieId")
    fun updateFavouriteMovie(movieId: Int, isFavourite: Int)


}