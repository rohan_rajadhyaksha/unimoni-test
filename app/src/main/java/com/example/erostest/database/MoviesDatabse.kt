package com.example.erostest.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.erostest.Model.Result

@Database(
    entities = [
        Result::class
    ], version = DbConstants.DB_VERSION, exportSchema = false
)
abstract class MoviesDatabase : RoomDatabase() {


    abstract fun moviesDao(): MoviesDao

    companion object {
        private lateinit var instance: MoviesDatabase

        fun getInstance(): MoviesDatabase {
            return instance
        }

        internal fun initPaymentDatabase(context: Context) {
            instance = Room.databaseBuilder(context.applicationContext, MoviesDatabase::class.java, DbConstants.DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }

}