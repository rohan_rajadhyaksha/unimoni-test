package com.example.erostest.database

import android.content.Context

fun initDatabases(context: Context) {
    MoviesDatabase.initPaymentDatabase(context)
}
