package com.example.erostest

enum class BenefitEnum(var typeString:String, var typeIcon:Int) {

    MOBILE("IHO_MOBILE_ICON",R.drawable.tele_consult),
    CHECKUP("IHO_CHECKUP_ICON",R.drawable.health_chk),
    PHARMACY("IHO_PHARMACY_ICON",R.drawable.medicine),
    DENTAL("IHO_DENTAL_ICON",R.drawable.dental_chk),
    DISCOUNT("IHO_DISCOUNT_CARD_ICON",R.drawable.discount_card);

    companion object BenefitEnumCompanion{

        fun getBenefitByBenefitName(typeString:String?):BenefitEnum?{
            return values().find { it.typeString == typeString }

        }
    }

}