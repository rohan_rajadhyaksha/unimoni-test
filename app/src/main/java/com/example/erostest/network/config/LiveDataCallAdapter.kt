package com.faasos.foodx.library.network.configuration


import android.util.Log
import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import com.example.erostest.BuildConfig
import com.example.erostest.constants.ErrorCodeConstants
import com.example.erostest.network.configuration.DataResponse
import com.example.erostest.network.configuration.ErrorResponse
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.reflect.Type
import java.net.SocketTimeoutException

class LiveDataCallAdapter<R> internal constructor(private val responseType: Type) :
    CallAdapter<R, LiveData<DataResponse<R>>> {

    override fun responseType(): Type {
        return responseType
    }

    override fun adapt(@NonNull call: Call<R>): LiveData<DataResponse<R>> {

        return object : LiveData<DataResponse<R>>() {
            override fun onActive() {
                super.onActive()

                call.clone().enqueue(object : Callback<R> {
                    override fun onResponse(@NonNull call1: Call<R>, @NonNull response: Response<R>) {
                        if (call1.isCanceled) return

                        if (response.isSuccessful) {
                            postValue(DataResponse(response.body()!!))
                        } else {
                            var errorResponse = ErrorResponse()

                            if (response.errorBody() != null) {

                                errorResponse = getErrorResponse(response.errorBody()!!)
                            }
                            errorResponse.errorCode = response.code()
                            postValue(DataResponse(response.code(), errorResponse))

                        }
                    }

                    override fun onFailure(@NonNull call1: Call<R>, @NonNull t: Throwable) {

                        if (BuildConfig.DEBUG) {
                            Log.e("JACKSON PARSING ERROR:", t.message)
                        }
                        if (call1.isCanceled) return
                        val errorResponse = ErrorResponse()

                        if (t is IOException || t is SocketTimeoutException) {
                            val networkErrorCode = ErrorCodeConstants.NETWORK
                            errorResponse.errorCode = networkErrorCode
                            postValue(DataResponse(networkErrorCode, errorResponse))
                        } else {
                            val badReqErrorCode = ErrorCodeConstants.BAD_REQUEST
                            errorResponse.errorCode = badReqErrorCode
                            postValue(DataResponse(badReqErrorCode, errorResponse))
                        }
                    }
                })
            }
        }
    }


    private fun getErrorResponse(responseBody: ResponseBody): ErrorResponse {
        val mapper = ObjectMapper()
        try {
            return mapper.readValue(responseBody.string(), ErrorResponse::class.java)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return ErrorResponse()

    }

}
