package com.example.erostest.network.helper

import android.os.AsyncTask
import com.example.erostest.network.configuration.DataResponse

internal class ApiDataStoringTask<ApiResponseType, DbResponseType>(
    private val apiResponseType: ApiResponseType,
    private val apiDataStoringHelper: ApiDataStoringHelper<ApiResponseType, DbResponseType>
) : AsyncTask<Void, Void, Void>() {

    override fun doInBackground(vararg voids: Void): Void? {
        apiDataStoringHelper.storeApiResult(apiResponseType)
        return null
    }

    override fun onPostExecute(aVoid: Void?) {
        // we specially request a new live data,
        // otherwise we will get immediately last cached value,
        // which may not be updated with latest results received from network.
        val liveData = apiDataStoringHelper.loadFromDb()
        apiDataStoringHelper.resultLiveData.addSource(liveData) { newData ->
            apiDataStoringHelper.resultLiveData.setValue(
                DataResponse(newData)
            )
        }
    }
}