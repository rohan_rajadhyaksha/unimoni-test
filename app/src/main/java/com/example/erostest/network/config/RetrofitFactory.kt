package com.faasos.foodx.library.network.configuration

import android.os.Build
import androidx.annotation.NonNull
import com.example.erostest.BuildConfig
import com.example.erostest.constants.BaseUrlConstants
import com.example.erostest.network.helper.UrlConstants
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

internal object RetrofitFactory {

    private var retrofit: Retrofit? = null

    //  Retrofit need it for building, will be handled dynamically with @Url annotation
    private val retrofitClient: Retrofit
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BaseUrlConstants.BASE_URL_API)
                    .client(okHttpClient)
                    .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                    .addCallAdapterFactory(LiveDataCallAdapterFactory.create())
                    .build()
            }
            return retrofit!!
        }

    private val okHttpClient: OkHttpClient
        @NonNull
        get() {
            val builder = OkHttpClient.Builder()
            builder.addInterceptor(httpLoggingInterceptor)
            builder.addInterceptor(queryParamInterceptor)
            return builder.build()
        }

    private// Request customization: add request headers
    val queryParamInterceptor: Interceptor
        get() = Interceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()

            //  ADD COMMON URL QUERY PARAMETERS
            val originalHttpUrl = original.url()
            val url = originalHttpUrl.newBuilder()
                .addQueryParameter(UrlConstants.API_KEY_NAME, UrlConstants.API_KEY)
                .build()
            requestBuilder.url(url)


            val request = requestBuilder.build()
            chain.proceed(request)
        }


    private val objectMapper: ObjectMapper
        @NonNull
        get() {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false)
            objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true)
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                objectMapper.registerModule(
                    com.fasterxml.jackson.module.kotlin.KotlinModule(
                        512,
                        nullToEmptyCollection = true,
                        nullToEmptyMap = true
                    )
                )
            } else {
                //  OUR OWN IMPLEMENTATION TO HANDLE JACKSON KOTLIN MODULE ISSUE ON 2.9.8
                objectMapper.registerModule(KotlinModule(512, nullToEmptyCollection = true, nullToEmptyMap = true))
            }
            return objectMapper
        }

    private val httpLoggingInterceptor: HttpLoggingInterceptor
        @NonNull
        get() {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            } else {
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
            }
            return httpLoggingInterceptor
        }


    fun <S> createService(serviceClass: Class<S>): S {
        return retrofitClient.create(serviceClass)
    }
}
