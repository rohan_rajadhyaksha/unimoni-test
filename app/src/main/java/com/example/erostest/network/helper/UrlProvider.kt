package com.example.erostest.network.helper

import android.net.Uri
import com.example.erostest.constants.BaseUrlConstants

object UrlProvider {



    fun getPolicyDetailsUrl(membersCount: Int): String {
        val uri = Uri.parse(BaseUrlConstants.BASE_URL_API)
            .buildUpon()
            .appendPath("IHOPolicyDetails")
            .appendQueryParameter("member_count", membersCount.toString())
            .build()
        return uri.toString()//"//api/comboSetProducts"
    }

    fun getTopRatedMovies(pageNumber: Int): String {
        return with(Uri.parse(BaseUrlConstants.BASE_URL_API).buildUpon()) {
            appendPath("movie")
            appendPath("top_rated")
            appendQueryParameter(UrlConstants.KEY_PAGE, pageNumber.toString())
            build()
            toString()


        }
    }
}