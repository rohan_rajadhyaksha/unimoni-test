package com.faasos.foodx.library.network.configuration

import com.example.erostest.network.retrofit.ApiService

object ServiceProvider {

    private var apiService: ApiService? = null

    fun getApiService(): ApiService {
        if (apiService == null) {
            apiService = RetrofitFactory.createService(ApiService::class.java)
        }
        return apiService!!
    }


}
