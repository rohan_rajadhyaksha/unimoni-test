package com.example.erostest.network.helper

import android.content.Context
import android.net.ConnectivityManager
import com.example.erostest.ErosNowTestApplication


object InternetConnectionStatus {

    val isConnected: Boolean
        get() {
            var connected: Boolean
            try {
                val connectivityManager =
                    ErosNowTestApplication.application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectivityManager.activeNetworkInfo
                connected = networkInfo != null && networkInfo.isConnected
                return connected


            } catch (e: Exception) {
                // Let Retrofit handle it
                connected = true
            }

            return connected
        }
}
