package com.example.erostest.network.configuration

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
class ErrorResponse {

    @JsonProperty("code")
    var code: Int = -1

    @JsonProperty("error_code")
    var errorCode: Int = -1

    @JsonProperty("business_error_code")
    var businessErrorCode: Int = -1

    @JsonProperty("message")
    var message: String? = null

    @JsonProperty("success")
    var success: String? = null

    @JsonProperty("status")
    var status: String? = null

}