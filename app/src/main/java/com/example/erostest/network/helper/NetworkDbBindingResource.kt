package com.example.erostest.network.helper

import androidx.annotation.MainThread
import androidx.annotation.Nullable
import androidx.lifecycle.MediatorLiveData
import com.example.erostest.constants.ErrorCodeConstants
import com.example.erostest.network.configuration.DataResponse
import com.example.erostest.network.configuration.ErrorResponse


abstract class NetworkDbBindingResource<ApiResponseType, DbResponseType>
@MainThread
protected constructor() : NetworkResource<ApiResponseType>(), ApiDataStoringHelper<ApiResponseType, DbResponseType> {
    // returns a LiveData that represents the resource
    final override val resultLiveData: MediatorLiveData<DataResponse<DbResponseType>> = MediatorLiveData()

    init {
        resultLiveData.value = DataResponse(DataResponse.Status.LOADING)
        if (this.shouldAlwaysFetchData()) {
            fetchFromNetwork()
        } else {
            val dbSource = this.loadFromDb()
            resultLiveData.addSource(dbSource) { data ->
                if (shouldRefreshData(data)) {
                    resultLiveData.removeSource(dbSource)
                    fetchFromNetwork()
                } else {
                    resultLiveData.setValue(DataResponse(data))
                }
            }
        }
    }

    // Called with the data in the database to decide whether it should be
    // fetched from the network.
    @MainThread
    protected abstract fun shouldRefreshData(@Nullable data: DbResponseType?): Boolean

    @MainThread
    protected abstract fun shouldAlwaysFetchData(): Boolean

    // Called when the fetch fails. The child class may want to reset components
    // like rate limiter.
    @MainThread
    private fun onFetchFailed(errorCode: Int, errorResponse: ErrorResponse?) {
        resultLiveData.value = DataResponse(errorCode, errorResponse!!)
    }

    private fun fetchFromNetwork() {
        if (InternetConnectionStatus.isConnected) {
            val apiResponse = createCall()
            resultLiveData.addSource(apiResponse) { response ->
                resultLiveData.removeSource(apiResponse)
                if (response != null) {
                    if (response.data != null) {
                        resultLiveData.value = DataResponse(DataResponse.Status.NETWORK_FETCH_SUCCESS)
                        saveResultAndReInit(response, this)
                    } else {
                        onFetchFailed(response.errorCode, response.errorResponse)
                    }
                } else {
                    sendNetworkFailure()
                }
            }
        } else {
            sendNetworkFailure()
        }
    }

    private fun sendNetworkFailure() {
        val errorResponse = ErrorResponse()
        errorResponse.errorCode = ErrorCodeConstants.NETWORK
        onFetchFailed(ErrorCodeConstants.NETWORK, errorResponse)
    }

    @MainThread
    private fun saveResultAndReInit(
        response: DataResponse<ApiResponseType>,
        apiDataStoringHelper: ApiDataStoringHelper<ApiResponseType, DbResponseType>
    ) {
        ApiDataStoringTask(response.data!!, apiDataStoringHelper).execute()
    }
}