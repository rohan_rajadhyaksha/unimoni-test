package com.example.erostest.network.helper

import androidx.annotation.MainThread
import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.erostest.constants.ErrorCodeConstants
import com.example.erostest.network.configuration.DataResponse
import com.example.erostest.network.configuration.ErrorResponse

abstract class NetworkResource<ApiResponseType>
@MainThread
protected constructor() {

    val apiResultLiveData: MediatorLiveData<DataResponse<ApiResponseType>> = MediatorLiveData()

    init {
        fetchFromNetwork()
    }

    // Called to create the API call.
    @NonNull
    @MainThread
    protected abstract fun createCall(): LiveData<DataResponse<ApiResponseType>>

    // Called when the fetch fails. The child class may want to reset components
    // like rate limiter.
    @MainThread
    private fun onFetchFailed(errorCode: Int, errorResponse: ErrorResponse?) {
        //apiResultLiveData.postValue(DataResponse(errorCode, errorResponse!!))
        apiResultLiveData.value = DataResponse(errorCode, errorResponse!!)
    }

    private fun fetchFromNetwork() {
        if (InternetConnectionStatus.isConnected) {
            //apiResultLiveData.postValue(DataResponse(DataResponse.Status.LOADING))
            apiResultLiveData.value = DataResponse(DataResponse.Status.LOADING)
            val apiResponse = createCall()
            apiResultLiveData.addSource(apiResponse) { response ->
                apiResultLiveData.removeSource(apiResponse)
                if (response != null) {
//                    if (response.data != null) {
                    apiResultLiveData.setValue(response)
//                    } else {
//                        onFetchFailed(response.errorCode, response.errorResponse)
//                    }
                } else {
                    sendNetworkFailure()
                }
            }
        } else {
            sendNetworkFailure()
        }
    }

    private fun sendNetworkFailure() {
        val errorResponse = ErrorResponse()
        errorResponse.errorCode = ErrorCodeConstants.NETWORK
        onFetchFailed(ErrorCodeConstants.NETWORK, errorResponse)
    }
}
