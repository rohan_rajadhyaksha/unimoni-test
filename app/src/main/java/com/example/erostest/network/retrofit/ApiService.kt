package com.example.erostest.network.retrofit

import androidx.lifecycle.LiveData
import com.example.erostest.Model.PolicyDetails
import com.example.erostest.Model.TopRatedMovieResponse
import com.example.erostest.network.configuration.DataResponse
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiService {

    @GET
    fun getPolicyDetails(@Url url: String): LiveData<DataResponse<PolicyDetails>>

    @GET
    fun getTopRatedMovies(@Url url: String): LiveData<DataResponse<TopRatedMovieResponse>>


}



