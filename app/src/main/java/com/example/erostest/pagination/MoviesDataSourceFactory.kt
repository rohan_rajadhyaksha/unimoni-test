package com.example.erostest.pagination

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.erostest.Model.Result

class MoviesDataSourceFactory : DataSource.Factory<Int, Result>() {

    val liveData: MutableLiveData<MoviesDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, Result> {
        val movieDataSource = MoviesDataSource()
        liveData.postValue(movieDataSource)
        return movieDataSource
    }
}