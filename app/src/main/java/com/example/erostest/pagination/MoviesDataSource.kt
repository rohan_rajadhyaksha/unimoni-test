package com.example.erostest.pagination

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.PageKeyedDataSource
import com.example.erostest.Model.Result
import com.example.erostest.managers.MovieManager
import com.example.erostest.network.configuration.DataResponse

class MoviesDataSource : PageKeyedDataSource<Int, Result>() {
    var sourceIndex: Int = 1
    var observer: Observer<DataResponse<List<Result>>>? = null
    var dataLoadingStatus: MutableLiveData<DataResponse.Status> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Result>) {

        val topRatedMoviesLiveData = MovieManager.getTopRatedMovies(sourceIndex)

        observer = Observer {
            when (it.status) {
                DataResponse.Status.ERROR -> {
                    topRatedMoviesLiveData.removeObserver(observer!!)
                    dataLoadingStatus.postValue(it.status)
                }
                DataResponse.Status.LOADING -> {
                    dataLoadingStatus.postValue(it.status)

                }
                DataResponse.Status.SUCCESS -> {
                    dataLoadingStatus.postValue(it.status)
                    topRatedMoviesLiveData.removeObserver(observer!!)
                    val topRatedMovies = it.data
                    topRatedMovies?.let { response ->
                        sourceIndex++
                        callback.onResult(response, null, sourceIndex)

                    }
                }

                else -> {

                }


            }
        }
        topRatedMoviesLiveData.observeForever(observer!!)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Result>) {
        val topRatedMoviesLiveData = MovieManager.getTopRatedMovies(sourceIndex)

        observer = Observer {
            when (it.status) {
                DataResponse.Status.ERROR -> {
                    topRatedMoviesLiveData.removeObserver(observer!!)
                    dataLoadingStatus.postValue(it.status)
                }
                DataResponse.Status.LOADING -> {
                    dataLoadingStatus.postValue(it.status)

                }
                DataResponse.Status.SUCCESS -> {
                    dataLoadingStatus.postValue(it.status)
                    topRatedMoviesLiveData.removeObserver(observer!!)
                    val topRatedMovies = it.data
                    topRatedMovies?.let { response ->
                        sourceIndex++
                        callback.onResult(response, params.key + 1)



                    }
                }

                else -> {

                }


            }
        }
        topRatedMoviesLiveData.observeForever(observer!!)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Result>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}