package com.example.erostest.Model

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Plan(
    val benefits: MutableList<Benefit>,
    val plan_cost: Int,
    val plan_disc_cost: Int,
    val plan_name: String,
    val plan_save_upto: Int
)
