package com.example.erostest.Model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.example.erostest.database.DbConstants
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(tableName = DbConstants.MOVIE_TABLE)
class Result {

    var adult: Boolean? = null
    var backdrop_path: String? = null
    @Ignore
    @JsonIgnore
    var genre_ids: List<Int>? = null
    @PrimaryKey
    var id: Int = 0
    var original_language: String? = null
    var original_title: String? = null
    var overview: String? = null
    var popularity: Double? = null
    var poster_path: String? = null
    var release_date: String? = null
    var title: String? = null
    var video: Boolean? = null
    var vote_average: Double? = null
    var vote_count: Int? = null
    var favourite: Int = 0


    override fun equals(obj: Any?): Boolean {
        if (obj === this)
            return true

        val article = obj as Result?
        return article!!.id == this.id
    }


}