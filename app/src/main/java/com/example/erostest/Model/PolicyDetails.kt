package com.example.erostest.Model

import com.fasterxml.jackson.annotation.JsonInclude


@JsonInclude(JsonInclude.Include.NON_NULL)
data class PolicyDetails(
    val plans: MutableList<Plan>,
    val responseStatus: Int
)
