package com.example.erostest.Model

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Benefit(
    val benefit_icon: String? = null,
    val benefit_icon_local: String? = null,
    val benefit_icon_network: String? = null,
    val benefit_sub_title: String? = null,
    val benefit_title: String? = null
)